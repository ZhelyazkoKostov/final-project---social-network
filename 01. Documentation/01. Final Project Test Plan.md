# **Test Plan**

> ### Social Network <br>
>__Prepared by:__ *Zhelyazko Kostov and Nino Simeonov* <br>
>__Date:__ 23.10.2019 <br>
>__Document Version:__ 1.0

## **Contents**

1. **[INTRODUCTION](#introduction)**

    1.1. OBJECTIVES

2. **[SCOPE](#scope)**

    2.1. FEATURES TO BE TESTED 

    2.2. FEATURES NOT TO BE TESTED

3. **[APPROACH](#approach)**

4. **[TESTING PROCESS](#testing_process)**

    4.1. TEST DELIVERABLES

    4.2. RESPONSIBILITIES

    4.3. RESOURCES

    4.4. ESTIMATION AND SCHEDULE

5. **[ENVIRONMENT REQUIREMENTS](#env_requirements)**

---

### **1. <a name="introduction"></a> Introduction**

The application under test (AUT) represents a Social Network concept - a website that brings people together to talk, share ideas and interests, or make new friends. It will contain all main functionalities and features related to a Social Network (SN) as:

- Search for people;

- Connect to people;

- Create, comment and like posts;

- Get a feed of the newest/most relevant posts of your connections;

- Change and update personal information;

- Administrative part with functionalities like:

    * Edit/Delete Profile;
    * Edit/Delete Post;
    * Edit/Delete Comments;

**1.1. Objectives**

The main objectives of the testing activities will be focusing on:

- Verifying the requirements are fulfilled by the application;

- Validate the application meets the customer expectations and needs;

- To create confidence in the product quality;

- To simulate the user actual flow;

- To identify the defects within the application;

---

### **2.	<a name="scope"></a>Scope**

The scope of the project will include:

- Testing of the web services, using Postman to automate the test cases. The APIs will be  accessed from Swagger. After creating the test cases, the automation will be done within 3 days;

- Testing of the main functionalities of the SN. Testing will include both, manual and automation testing techniques.

    * The automation part will be developed using Selenium. Some of the tests will be developed following BDD principles. After developing the test cases, the automation will be implemented within one week.

    * The manual testing will cover happy paths and main functionalities, as well as non-automating features.  The manual testing will be executed within two days.

- Database exploratory testing. The objectives are to verify correct and sensible connections between tables, correct and sensible values and constraints allocated. This testing will be done within one day.

- Preparation of detailed tests and bugs reports. The non- automating reports will be designed and created within three days.

**2.1. Features to be tested**

- Creation of a profile;

- Search, connect and disconnect from a profile;

- Create, like, unlike and delete a post. Set visibility status. Post likes counter;

- Create, like, unlike and delete, reply to a comment;

- Visibility and non-visibility of profile information (depending on profile settings);

- Visibility of publicly set posts;

- Visibility of public feeds;

- Personal profile information update:

    * Change name;
	* Upload profile picture. Set visibility status;
	* Update age, e-mail, nationality.

- Generation of personal post feeds. Sort order;

- Administration properties:

    * Edit/delete profile;
	* Edit/delete post;
	* Edit/delete comment.

**2.2. Features not to be tested**

The following are considered out of scope for Social Network system Test Plan and testing scope:

- Functional requirements testing for systems outside Social Network application

- Testing of Business Standard Operating Procedures, disaster recovery and Business Continuity Plan.

---

### **3.	 <a name="approach"></a>Approach**
Exploratory manual testing will be performed on the main features and happy paths, using end-to-end test cases;

**3.1. Registration part**

The registration testing approach will mostly include automated tests and a few manual tests.

The automation will cover а few positive and negative options of the registration phase.

**3.2. Public part**

The approach for testing the Public part will be to test a few features manually and a few using Selenium WebDriver 

**3.3. Private part**

- Profile settings and updates;

- Connections;

- Posts;

- Comments;

- Posts feed;

The approach for testing the Private part will comprise both manual and automation testing;

In this section, few happy paths will be covered with automation testing. Automation testing will also be used for the main functionalities for all subsections excluding the Posts feed, where manual testing techniques will be used.

Here will be performed functional testing for all main features, integration testing for the correct connection and transition of the separate elements.

It will be used boundary value analysis for posts and comments, equivalence partitioning for public and private settings, state transition testing for major functionality options.

Finally, if time is left will be used usability testing to verify the ease of the application exploit.

For this section will be used some test cases developed on the BDD concept.

**3.4.Administrative part**

The approach for testing the Administrative part will comprise both manual and automation testing;

---

### **4. <a name="testing_process"></a>Testing Process**

**4.1. Test Deliverables:**

Testing will provide specific deliverables during the project.  These deliverables fall into three basic categories: Documents, Test Cases / Bug Write-ups, and Reports.  

**4.2. Responsibilities:**

|**Responsibility**|**Responsible**|
|------------------|---------------|
| Exploratory testing |Both QA Members |
|Test Cases design	|Both QA Members|
|Public Part Manual Testing	|Both QA Members|
|Registration Part Automation Code Design|Zhelyazko Kostov|
|Private Part Automation Code Design|Both QA Members|
|Administrative Part Automation Code Design|Nino Simeonov|
|Usability Testing|Both QA Members|
|Rest API Testing Automation Code Design|Both QA Members|
|Automation Execution and fixtures|Both QA Members|
|Report design and generation|Both QA Members|
|Bug Creation and Closure|Both QA Members|

**4.3. Resources:**

|||
|---|---|
|JDE	|Intellij|
|Automation Testing Design and Execution	|Selenium|
|BDD Testing Design and Execution	|JBehave|
|Rest API Testing	|Postman|
|Source Code Management Tool	|Gitlab|
|Bug Tracking Tool	|Gitlab|
|Test Case Creation	|TestRail|
|Reporting	|TestRail|
|Progress Communicator	|Trello|

**4.4. Estimation and Schedule:**

|**Task**|**Effort**|
|---|---|
|1. Test Cases design	|4 days|
|2. Rest API Testing Automation Code Design	|3 days|
|3. The Exploratory Testing	|2 days|
| - *For Public part*	|4 hours|
| - *For the Registration part*|1.5 hours|
| - *For the Private part*	|8 hours|
| - *For the Administrative part*	|2.5 hours|
|4. Public Part Manual Testing	|2 days|
|5. Registration Part Automation Code Design	|2 days|
|6. Private Part Automation Code Design	|7 days|
|7. Administrative Part Automation Code Design	|2 days|
|8. Usability Testing	|1 day|
|9. Automation Execution and fixtures	|3 days|
|10. Report design and generation	|2 days|
|11. Bug Creation and Closure	|ongoing|

---

### **5. <a name="env_requirements"></a>Environment Requirements**

**PC 1**

|||
|---|---|
|OS Name	|Windows 7 Professional|
|Version	|6.1.7601 Service Pack 1 Build 7601|
|System Model	|HP Pavilion Notebook|
|System Type	|X64-based PC|
|Processor	|Intel® Core™ i5-5200U CPU @ 2.20GHz|
|RAM	|8 GB|
|Resolution	|1920 x 1080|
|Scale	|100 %|

**PC 2**

|||
|---|---|
|OS Name|Windows 10 Home |
|Version	| 1903 |
|System Model	| Dell Vostro|
|System Type	|X64-based PC|
|Processor	|Intel® Core™ i7-8550U CPU @ 1.80GHz|
|RAM	|8 GB|
|Resolution	|1920 x 1080|
|Scale	|100 %|


**Browser**	

|||
|---|---|
|Chrome	Version |78.0.3904.70 (Official Build) (64-bit)|
|Chrome	Version | 78.0.3904.108 (Official Build) (64-bit)|

