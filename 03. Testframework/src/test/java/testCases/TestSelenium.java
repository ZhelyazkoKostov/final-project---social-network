package testCases;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSelenium extends BaseTest {

    @Test
    public void TC001_UnregisterUserSearchUsersByName() {
        actions.waitForElementVisible("searchField", 20);
        actions.typeValueInFieldByConfProperty("existingUsername", "searchField");
        actions.clickElement("searchButton");
        actions.waitForElementVisible("firstSearchResult", 20);
        actions.assertElementPresent("firstSearchResult");
    }

    @Test
    public void TC002_UnregisterUserClearSearch() {
        actions.waitForElementVisible("searchField", 20);
        actions.typeValueInFieldByConfProperty("existingUsername", "searchField");
        actions.clickElement("searchButton");
        actions.waitForElementVisible("firstSearchResult", 20);
        actions.clickElement("searchClearButton");
        actions.waitForElementVisible("firstSearchResultMailM", 20);
        actions.assertElementPresent("firstSearchResultMailM");
    }

    @Test
    public void TC003_UnregisterUserSearchUsersByEmail() {
        actions.waitForElementVisible("searchField", 20);
        actions.typeValueInFieldByConfProperty("existingEmail", "searchField");
        actions.clickElement("searchButton");
        actions.waitForElementVisible("firstSearchResultMail", 20);
        actions.assertElementPresent("firstSearchResultMail");
    }

    @Test
    public void TC004_EditProfileOptionalInfo() {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("accountSettingsButton", 20);
        actions.clickElement("accountSettingsButton");
        actions.waitForElementVisible("editProfileButton", 20);
        actions.clickElementJS("editProfileButton");
        actions.waitForElementVisible("firstNameFieldEditPage", 20);
        actions.typeValueInFieldByConfProperty("newEditUsername", "firstNameFieldEditPage");
        actions.typeValueInFieldByConfProperty("newEditUsername", "lastNameFieldEditPage");
        actions.typeValueInFieldByConfProperty("age", "ageFieldEditPage");
        actions.typeValueInFieldByConfProperty("jobTitle", "jobTitleFieldEditPage");
        actions.typeTextJS("https://drive.google.com/open?id=1rvBzTD4l3hPe2_lN848nDQefLAJ2m2XX", "editSongField");
        actions.waitForElementVisible("saveEditPageButton", 20);
        actions.clickElementJS("saveEditPageButton");
        actions.waitForElementVisible("messageUpdateProfile", 20);
        actions.assertElementPresent("messageUpdateProfile");
        actions.clearFieldsUser();
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");

    }

    @Test
    public void TC005_EditProfilePassword() {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("username6", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("accountSettingsButton", 20);
        actions.clickElement("accountSettingsButton");
        actions.waitForElementVisible("editProfileButton", 20);
        actions.clickElementJS("editProfileButton");
        actions.waitForElementVisible("posswordFieldEditPage", 20);
        actions.typeValueInFieldByConfProperty("newEditPassword", "posswordFieldEditPage");
        actions.waitForElementVisible("saveEditPageButton", 20);
        actions.clickElementJS("saveEditPageButton");
        actions.waitForElementVisible("messageUpdateProfile", 20);
        actions.assertElementPresent("messageUpdateProfile");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("username6", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.waitForElementVisible("loginButton", 20);
        actions.clickElementJS("loginButton");
        actions.waitForElementVisible("loginMessage", 20);
        actions.assertElementPresent("loginMessage");
        actions.typeValueInFieldByConfProperty("username6", "usernameField");
        actions.typeValueInFieldByConfProperty("newEditPassword", "passwordField");
        actions.clickElement("loginButton");
        actions.clearFieldsUser();
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }

    @Test
    public void TC006_UserSuccessfullАcceptFriendRequests() {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElementJS("loginButton");
        actions.waitForElementVisible("viewProfileButtonUser6", 20);
        actions.clickElement("viewProfileButtonUser6");
        actions.waitForElementVisible("sendRequestUser6", 20);
        actions.clickElementJS("sendRequestUser6");
        actions.waitForElementVisible("commentModifyMenuButton", 20);
        actions.waitForElementVisible("friendsRequestUserMenu", 20);
        actions.assertElementPresent("friendsRequestUserMenu");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
        actions.typeValueInFieldByConfProperty("username6", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("accountSettingsButton", 20);
        actions.clickElementJS("accountSettingsButton");
        actions.waitForElementClickable("friendsAcceptButton", 20);
        actions.clickElementJS("friendsAcceptButton");
        actions.waitForElementVisible("friendsAcceptUserMenu", 20);
        actions.assertElementPresent("friendsAcceptUserMenu");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");

    }

    @Test
    public void TC007_UserSuccessfullDelleteFriend()  {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("accountSettingsButton", 20);
        actions.clickElementJS("accountSettingsButton");
        actions.clickElementJS("friendsDeclineButton");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }

    @Test
    public void TC008_SuccessfullCreateComentInPost() throws InterruptedException {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("newsIcon", 20);
        actions.clickElementJS("newsIcon");
        actions.waitForElementVisible("newPostTextarea", 20);
        actions.typeTextJS("newPostBodyText", "newPostTextarea");
        actions.clickElementJS("sharePost");
        Thread.sleep(2000);
        actions.clickElementJS("lastComentTextareaField");
        actions.waitForElementVisible("getMessageTextFirst", 20);
        actions.verifyTextEquals("newPostBodyText", "getMessageTextFirst");
        actions.assertTextEquals("newPostBodyText", "getMessageTextFirst");
        Thread.sleep(2000);
        actions.waitForElementVisible("lastComentTextareaField", 20);
        actions.clickElementJS("lastComentTextareaField");
        actions.typeTextJS("newPostBodyText", "lastComentTextareaField");
        actions.clickElementJS("lastComentTextareaField");
        actions.hitENTER("lastComentTextareaField");
        actions.waitForElementVisible("getMessageTextSecond", 20);
        actions.verifyTextEquals("newPostBodyText", "getMessageTextSecond");
        actions.assertTextEquals("newPostBodyText", "getMessageTextSecond");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }

    @Test
    public void TC009_UserSuccessfullyEditedAComment() {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("logoutButton", 20);
        actions.waitForElementVisible("accountSettingsButton", 20);
        actions.clickElement("accountSettingsButton");
        actions.waitForElementVisible("commentModifyMenuButton", 20);
        actions.clickElementJS("commentModifyMenuButton");
        actions.clickElementJS("commentEditButton");
        actions.waitForElementVisible("commentEditTextarea", 20);
        actions.clickElement("commentEditTextarea");
        actions.waitForElementVisible("commentModifyMenuButton", 20);
        actions.clickElement("commentModifyMenuButton");
        actions.waitForElementVisible("commentEditTextarea", 20);
        actions.clearFieldText("commentEditTextarea");
        actions.clickElement("commentModifyMenuButton");
        actions.waitForElementVisible("commentEditTextarea", 20);
        actions.typeValueInFieldByConfProperty("commentEditText", "commentEditTextarea");
        actions.waitForElementVisible("commentModifyMenuButton", 20);
        actions.clickElement("commentModifyMenuButton");
        actions.waitForElementVisible("commentSaveButton", 20);
        actions.clickElement("commentSaveButton");
        actions.waitForElementVisible("getMessageText", 20);
        actions.verifyTextEquals("This Comments has been edited! -", "getMessageText");
        actions.assertTextEquals("This Comments has been edited! -", "getMessageText");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }

    @Test
    public void TC010_AdminSuccessfullEditPost() {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.clickElement("publicPostsButton");
        actions.waitForElementVisible("publicAdminLink", 20);
        actions.clickElement("publicAdminLink");
        actions.waitForElementVisible("AdminEditPostDropdoun", 20);
        actions.clickElementJS("AdminEditPostDropdoun");
        actions.clickElementJS("AdminEditPostEdit");
        actions.waitForElementVisible("AdminEditPostTextField", 20);
        actions.clearFieldText("AdminEditPostTextField");
        actions.typeValueInField("editPostBodyText", "AdminEditPostTextField");
        actions.clickElementJS("AdminEditPostVisibilityPublic");
        actions.clickElementJS("saveEditPageButton");
        actions.waitForElementVisible("mostLikedPostsOfYourFriendButton", 20);
        actions.clickRecentPostButton("mostLikedPostsOfYourFriendButton");
        actions.waitForElementVisible("getMessageTextFirst", 20);
        actions.verifyTextEquals("editPostBodyText", "getMessageTextFirst");
        actions.assertTextEquals("editPostBodyText", "getMessageTextFirst");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }




    @Test
    public void TC011_SuccessfullUploadVideoInPost() throws InterruptedException {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("newsIcon", 20);
        actions.clickElementJS("newsIcon");
        actions.waitForElementVisible("newPostTextarea", 20);
        actions.typeTextJS("newPostBodyText", "newPostTextarea");
        actions.waitForElementVisible("postVideoButton", 20);
        actions.clickElementJS("postVideoButton");
        actions.fileUplood("https://www.youtube.com/watch?v=AhfW-4ulolk&list=RDAhfW-4ulolk&start_radio=1&t=826","postVideoField");
        Thread.sleep(2000);
        actions.clickElement("sharePost");
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElementJS("logoutButton");
    }


    @Test
    public void TC012_SuccessfullDeleteVideoInPost() throws InterruptedException {
        actions.waitForElementVisible("usernameField", 20);
        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
        actions.typeValueInFieldByConfProperty("password", "passwordField");
        actions.clickElement("loginButton");
        actions.waitForElementVisible("newsIcon", 20);
        actions.clickElementJS("newsIcon");
        actions.clickRecentPostButton("mostLikedPostsOfYourFriendButton");
        actions.waitForElementVisible("FirstPostButton", 20);
        actions.clickElementJS("FirstPostButton");
        actions.clickElementJS("deletePostButton");
        actions.waitForElementVisible("yesConfirmDeleteButton", 20);
        actions.clickElementJS("yesConfirmDeleteButton");
        actions.waitForElementVisible("okDeleteButton", 20);
        actions.clickElementJS("okDeleteButton");
        Thread.sleep(2000);
        actions.waitForElementVisible("logoutButton", 20);
        actions.clickElement("logoutButton");
    }


//    @Test
//    public void TC004_EditProfileUsername() {
//        actions.typeValueInFieldByConfProperty("userForLogin","usernameField");
//        actions.typeValueInFieldByConfProperty("password","passwordField");
//        actions.clickElement("loginButton");
//        actions.waitForElementVisible("viewProfileButtonUser4",10);
//        actions.clickElement("viewProfileButtonUser4");
//        actions.waitForElementVisible("mailEditetUserConfurm",10);
//        UserActions.editPage();
//        actions.waitForElementVisible("usernameFieldEditPage",10);
//        actions.typeValueInFieldByConfProperty("newEditUsername","usernameFieldEditPage");
//        actions.clickElement("saveEditPageButton");
//        actions.waitForElementVisible("messageUpdateProfile",10);
//        actions.assertElementPresent("messageUpdateProfile");
//
//    }



    //    @Test
//    public void TC010_SuccessfullUploadPictureInPost() throws InterruptedException {
//        actions.waitForElementVisible("usernameField", 20);
//        actions.typeValueInFieldByConfProperty("userForLogin", "usernameField");
//        actions.typeValueInFieldByConfProperty("password", "passwordField");
//        actions.clickElement("loginButton");
//        actions.waitForElementVisible("newsIcon", 20);
//        actions.clickElementJS("newsIcon");
//        actions.waitForElementVisible("newPostTextarea", 20);
//        actions.typeTextJS("newPostBodyText", "newPostTextarea");
//        actions.waitForElementVisible("imegeTabPost", 20);
//        actions.clickElementJS("imegeTabPost");
//        actions.fileUplood("G:\\Final Project - Telerik Academy Alpha-QA-May-2019\\final-project\\testframework\\src\\test\\resources\\mappings\\joeJ1.jpg","postPictureButton");
//        Thread.sleep(2000);
//        actions.clickElement("sharePost");
//        actions.waitForElementVisible("logoutButton", 20);
//        actions.clickElementJS("logoutButton");
//    }
}
