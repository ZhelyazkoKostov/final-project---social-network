package stepdefinitions;

import Pages.Comments;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

public class CommentStepDefinitions extends BaseStepDefinitions{

    UserActions actions = new UserActions();
    Comments comments = new Comments();

    @When("User comments Friend Post")
    public void createNewComment(){
        comments.createPostComment();

    }

    @Then("The Comments is Displayed below the Post")
    public void assertCommentIsListed(){
        comments.assertCommentNotVisible();
    }

    @When ("User edits a Comment")
    public void editComment(){
        comments.editComment();
    }

    @Then ("Comment with $text is listed as $newestComment")
    public void checkPostNotListedInPostFeed(String text, String location){
        actions.waitForElementVisible(location,10);
        actions.assertTextNotEquals(Utils.getConfigPropertyByKey(text),location);
    }

    @When ("User deletes Comment")
    public void deleteComment(){
        comments.deleteComment();
    }

    @Then ("Comment is deleted")
    public void assertCommentNotVisible(){
        comments.assertCommentNotVisible();

    }

}
