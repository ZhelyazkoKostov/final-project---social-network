package stepdefinitions;

import Pages.Login;
import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class PostStepDefinitions extends BaseStepDefinitions{

    UserActions actions = new UserActions();
    Login login = new Login();

    @Then ("Post with $text is listed as $newestPost on Post Feed")
    public void checkPostListedOnTopInPostFeed(String text, String location){
        actions.waitForElementVisible(location,10);
        actions.verifyTextEquals(text, location);
    }

    @Then ("Post with $text isn't listed as $newestPost at the top of the Post Feed")
    public void checkPostNotListedOnTopInPostFeed(String text, String location){
        String textbody = Utils.getConfigPropertyByKey(text);
        String locator = Utils.getUIMappingByKey(location);
        actions.waitForElementVisible(locator,10);
        actions.verifyTextEquals(textbody, locator);
    }

    @Then ("Post with $text is listed as $newestPost")
    public void checkPostListedInPostFeed(String text, String location){
        actions.waitForElementVisible(location,10);
        actions.assertTextEquals(Utils.getConfigPropertyByKey(text),location);
    }

    @Then ("Post with $text is not listed as $newestPost")
    public void checkPostNotListedInPostFeed(String text, String location){
        actions.waitForElementVisible(location,10);
        actions.assertTextNotEquals(Utils.getConfigPropertyByKey(text),location);
    }

    @Given("User is at Landing Page and loggs in")
    public void redirectToLandingPage(){
        boolean status = actions.verifyElementPresent("logoutButton");
        if(status){
            actions.clickElement("logoutButton");
            actions.waitForElementVisible("loginButton",10);
            login.login();
        }
        else {
            Utils.LOG.info("User is already Logged In");
            login.login();
        }
    }

    @Then("Success message is sent in the console")
    public void printConsoleMsg(){
        System.out.println("Successful Edit!");
    }

    @Then ("User returns to Landing Page")
    public void returnToLandingPage(){
        actions.clickElement("homePageButton");
    }






}
