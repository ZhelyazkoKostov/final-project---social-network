package Pages;

import com.telerikacademy.testframework.UserActions;
import com.telerikacademy.testframework.Utils;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Comments extends UserActions {

    final WebDriver driver;

    public Comments() {
        this.driver = Utils.getWebDriver();
    }



    public void createPostComment(){
        waitForElementVisible("viewJoe2Button",10);
        clickElement("viewJoe2Button");
        waitForElementVisible("commentTextarea",10);
        typeValueInFieldByConfProperty("newCommentBodyText","commentTextarea");
        hitENTER("commentTextarea");

    }
    public void assertCommentVisible(){
        String actCommentText;
        String expCommentText = Utils.getConfigPropertyByKey("newCommentBodyText");
        waitForElementVisible("postedCommentText",10);
        actCommentText = getText("postedCommentText");
        Assert.assertEquals(expCommentText,actCommentText);
    }

    public void assertCommentNotVisible(){
        String actCommentText;
        String expCommentText = Utils.getConfigPropertyByKey("commentEditText");
        waitForElementVisible("postedCommentText",10);
        actCommentText = getText("postedCommentText");
        Assert.assertNotEquals(expCommentText,actCommentText);
    }

    public void editComment(){
        try{Thread.sleep(2000);}catch (InterruptedException e){System.out.println(e.getMessage());}
        waitForElementVisible("viewJoe1Button",10);
        clickElement("viewJoe1Button");
        waitForElementVisible("commentModifyMenuButton",10);
        clickElement("commentModifyMenuButton");
        waitForElementVisible("commentEditButton",10);
        clickElement("commentEditButton");
            try{Thread.sleep(2000);}catch (InterruptedException ed){System.out.println(ed.getMessage());}
        waitForElementVisible("commentTextarea",10);
        typeValueInFieldByConfProperty("commentEditText","commentEditTextarea");
            try{Thread.sleep(2000);}catch (InterruptedException ew){System.out.println(ew.getMessage());}
        waitForElementVisible("commentModifyMenuButton",10);
        clickElement("commentModifyMenuButton");
            try{Thread.sleep(2000);}catch (InterruptedException et){System.out.println(et.getMessage());}
        waitForElementVisible("commentSaveButton",10);
        clickElement("commentSaveButton");

    }

    public void deleteComment(){
        waitForElementVisible("viewJoe1Button",10);
        clickElement("viewJoe1Button");
        waitForElementVisible("commentModifyMenuButton",10);
        clickElement("commentModifyMenuButton");
        waitForElementVisible("commentDeleteButton",10);
        clickElement("commentDeleteButton");
        waitForElementVisible("yesConfirmDeleteButton",10);
        clickElement("yesConfirmDeleteButton");
        waitForElementVisible("okDeleteButton",10);
        clickElement("okDeleteButton");
    }

    public void typeNewText(String newText, String locator){
        WebElement location = createElement(locator);
        location.sendKeys(newText);
        WebElement field = createElement("commentEditTextarea");
        field.sendKeys(newText);
    }
}
