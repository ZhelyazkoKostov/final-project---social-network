package com.telerikacademy.testframework;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import io.github.bonigarcia.wdm.FirefoxDriverManager;


public class CustomWebDriverManager {
	public enum CustomWebDriverManagerEnum {
		INSTANCE;
		String browser = Utils.getConfigPropertyByKey("browser");
		private WebDriver driver = setupFirefoxBrowser();



		private WebDriver setupFirefoxBrowser() {
			if (browser.equals("firefox")) {
				FirefoxDriverManager.firefoxdriver().setup();
				WebDriver firefoxDriver = new FirefoxDriver();
				firefoxDriver.manage().window().maximize();
				return firefoxDriver;
			} else if (browser.equals("chrome")) {
				ChromeOptions opts = new ChromeOptions();
				opts.addArguments("-incognito");
				ChromeDriverManager.chromedriver().setup();
				driver = new ChromeDriver(opts);
				driver.manage().window().maximize();
				return driver;
			}
			return null;
		}

		public void quitDriver() {
			if (driver != null) {
				driver.quit();
			}
		}

		public WebDriver getDriver() {
			return driver;
		}


	}
}
