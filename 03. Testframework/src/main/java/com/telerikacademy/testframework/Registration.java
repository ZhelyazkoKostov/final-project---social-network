package com.telerikacademy.testframework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Registration extends UserActions{

    final WebDriver driver;

    public Registration() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser() {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey("base.url"));
    }

    public static void quitDriver(){
        Utils.tearDownWebDriver();
    }

    public void assertConfirmationMessage(String expText, String location){
        String alertSuccessRegistration = Utils.getUIMappingByKey(location);
        System.out.println(location + " " + alertSuccessRegistration);
        waitForElementVisible(alertSuccessRegistration,10);
        assertTextEquals(Utils.getConfigPropertyByKey(expText), alertSuccessRegistration);

    }

}
