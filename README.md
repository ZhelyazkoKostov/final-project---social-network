# Final Project QA Telerik Academy - Social Network

### 1. Trello link: https://trello.com/b/xjaTUmOP/testcasesforsoclianetwork

### 2. GitLab link: https://gitlab.com/NinoSimeonov/final-project.git

### 3. TestRail link: https://howtoqa.testrail.io/index.php?/projects/overview/1

### 4. DEV Project link: https://gitlab.com/RosenAn/telerik-academy-final-project---social-network.git

### 5. Issues link : https://gitlab.com/RosenAn/telerik-academy-final-project---social-network/issues